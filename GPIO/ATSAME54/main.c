//-----------------------------------------------------------------------------
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include "same54.h"

//-----------------------------------------------------------------------------
static void sys_init(void)
{
  NVMCTRL->CTRLA.reg = NVMCTRL_CTRLA_RWS(5);

  OSC32KCTRL->XOSC32K.reg = OSC32KCTRL_XOSC32K_ENABLE | OSC32KCTRL_XOSC32K_XTALEN |
      OSC32KCTRL_XOSC32K_EN32K | OSC32KCTRL_XOSC32K_RUNSTDBY | OSC32KCTRL_XOSC32K_STARTUP(7);
  while (0 == OSC32KCTRL->STATUS.bit.XOSC32KRDY);

  #define LDR (((unsigned long)F_CPU * 32) / 32768)

  GCLK->GENCTRL[1].reg = GCLK_GENCTRL_SRC(GCLK_SOURCE_XOSC32K) | GCLK_GENCTRL_RUNSTDBY | GCLK_GENCTRL_GENEN;

  GCLK->PCHCTRL[OSCCTRL_GCLK_ID_FDPLL0].reg = GCLK_PCHCTRL_GEN(1) | GCLK_PCHCTRL_CHEN;
  while (0 == (GCLK->PCHCTRL[OSCCTRL_GCLK_ID_FDPLL0].reg & GCLK_PCHCTRL_CHEN));

  GCLK->PCHCTRL[OSCCTRL_GCLK_ID_FDPLL032K].reg = GCLK_PCHCTRL_GEN(1) | GCLK_PCHCTRL_CHEN;
  while (0 == (GCLK->PCHCTRL[OSCCTRL_GCLK_ID_FDPLL032K].reg & GCLK_PCHCTRL_CHEN));

  OSCCTRL->Dpll[0].DPLLRATIO.reg = OSCCTRL_DPLLRATIO_LDRFRAC(LDR % 32) |
      OSCCTRL_DPLLRATIO_LDR((LDR / 32) - 1);
  OSCCTRL->Dpll[0].DPLLCTRLB.reg = OSCCTRL_DPLLCTRLB_REFCLK_XOSC32 |
      OSCCTRL_DPLLCTRLB_DIV(1) | OSCCTRL_DPLLCTRLB_WUF | OSCCTRL_DPLLCTRLB_LBYPASS;
  OSCCTRL->Dpll[0].DPLLCTRLA.reg = OSCCTRL_DPLLCTRLA_ENABLE | OSCCTRL_DPLLCTRLA_RUNSTDBY;

  while (0 == OSCCTRL->Dpll[0].DPLLSTATUS.bit.CLKRDY || 0 == OSCCTRL->Dpll[0].DPLLSTATUS.bit.LOCK);

  GCLK->GENCTRL[0].reg = GCLK_GENCTRL_SRC(GCLK_SOURCE_DPLL0) |
      GCLK_GENCTRL_RUNSTDBY | GCLK_GENCTRL_GENEN;
}


//-----------------------------------------------------------------------------
__attribute__ ((noinline, section(".ramfunc")))
void ram_test(void)
{
  asm(".align 5");
//  asm("nop");
//  asm("nop");

  while (1)
  {
    if (PORT->Group[1].IN.reg & (1 << 4))
      PORT->Group[0].OUTCLR.reg = (1 << 6);
    else
      PORT->Group[0].OUTSET.reg = (1 << 6);
  }
}

//-----------------------------------------------------------------------------
__attribute__ ((noinline, section(".ramfunc")))
void ram_test1(void)
{
  asm(".align 5");
//  asm("nop");
/*
  asm("nop");
  asm("nop");
  asm("nop");
*/
  while (1)
  {
    PORT->Group[0].OUTTGL.reg = (1 << 6);
  }
}

//-----------------------------------------------------------------------------
int main(void)
{
  sys_init();

  // OUT - A6
  // IN  - B4

  PORT->Group[0].DIRSET.reg = (1 << 6);
  //PORT->Group[0].CTRL.reg = 0xffffffff;
  //PORT->Group[0].PINCFG[6].reg = PORT_PINCFG_DRVSTR;

  PORT->Group[1].DIRCLR.reg = (1 << 4);
  PORT->Group[1].PINCFG[4].reg = PORT_PINCFG_INEN;
  //PORT->Group[1].CTRL.reg = 0xffffffff;

  CMCC->CTRL.reg = CMCC_CTRL_CEN;

  ram_test();

  asm("nop");
  asm("nop");
  asm("nop");

  asm(".align 5");
//  asm("nop");
//  asm("nop");
  asm("nop");
  asm("nop");

  asm("nop");
//  asm("nop");

#if 1
  while (1)
  {
    if (PORT->Group[1].IN.reg & (1 << 4))
      PORT->Group[0].OUTCLR.reg = (1 << 6);
    else
      PORT->Group[0].OUTSET.reg = (1 << 6);
  }
#else
  while (1)
  {
      PORT->Group[0].OUTTGL.reg = (1 << 6);
  }
#endif

  return 0;
}


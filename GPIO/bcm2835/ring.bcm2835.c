//
// ring.bcm2835.c
//
// bcm2835 ring oscillator test
//    gcc ring.bcm2835.c -o ring.bcm2835 -l bcm2835
//
// Neil Gershenfeld 12/19/20
//
// This work may be reproduced, modified, distributed,
// performed, and displayed for any purpose, but must
// acknowledge this project. Copyright is retained and
// must be preserved. The work is provided as is; no
// warranty is provided, and users accept all liability.
//
//
#include <bcm2835.h>
#include <stdio.h>
#define pinout RPI_GPIO_P1_16
#define pinin RPI_GPIO_P1_18
int main(int argc,char **argv) {
   bcm2835_init();
   bcm2835_gpio_fsel(pinout,BCM2835_GPIO_FSEL_OUTP);
   bcm2835_gpio_fsel(pinin,BCM2835_GPIO_FSEL_INPT);
   while (1) {
      if (bcm2835_gpio_lev(pinin) == LOW)
         bcm2835_gpio_write(pinout,HIGH);
      else
         bcm2835_gpio_write(pinout,LOW);
      }
   }

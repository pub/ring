//
// ring.ino
//
// ring oscillator test
//
// Neil Gershenfeld 8/12/21
//
// This work may be reproduced, modified, distributed,
// performed, and displayed for any purpose, but must
// acknowledge this project. Copyright is retained and
// must be preserved. The work is provided as is; no
// warranty is provided, and users accept all liability.
//

void setup() {
   pinMode(0,OUTPUT);
   }

void loop() {
   while (1) {
      /*
      if (digitalRead(1))
         digitalWrite(0,LOW);
      else
         digitalWrite(0,HIGH);
      */
      if (digitalReadFast(1))
         digitalWriteFast(0,LOW);
      else
         digitalWriteFast(0,HIGH);
      }
   }

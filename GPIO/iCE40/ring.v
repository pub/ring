//
// ring.v
//
// iCE40 FPGA ring oscillator test
//
// Erik Strand 9/30/21
//
// This work may be reproduced, modified, distributed,
// performed, and displayed for any purpose, but must
// acknowledge this project. Copyright is retained and
// must be preserved. The work is provided as is; no
// warranty is provided, and users accept all liability.
//

// Make yosys throw an error when we implicitly declare nets.
`default_nettype none

module top (
    input CLK_12MHZ, // the iCEBreaker has an external 12MHz oscillator
    output P1A1,
    input P1A2,
);
    wire clk;
    wire clk_lock;
    SB_PLL40_PAD #(
        .FEEDBACK_PATH("SIMPLE"),
        .PLLOUT_SELECT("GENCLK"),
        .FILTER_RANGE(3'b001),
        // == 24 MHz == (boring slow)
        //.DIVR(4'b0000),
        //.DIVF(7'b0111111),
        //.DIVQ(3'b101)
        // == 48 MHz == (getting interesting)
        //.DIVR(4'b0000),
        //.DIVF(7'b0111111),
        //.DIVQ(3'b100),
        // == 96 MHz == (knocks the socks off MCUs)
        //.DIVR(4'b0000),
        //.DIVF(7'b0111111),
        //.DIVQ(3'b011)
        // == 111 MHz == (fastest I can go while still passing timing tests)
        //.DIVR(4'b0000),
        //.DIVF(7'b1001001),
        //.DIVQ(3'b011)
        // == 120 MHz == (overclocked -- doesn't pass the icetime analysis, but seems stable)
        .DIVR(4'b0000),
        .DIVF(7'b1001111),
        .DIVQ(3'b011)
        // == 129 ==
        // (possibly unreliable -- once saw unstable oscillation, though it's worked since then)
        //.DIVR(4'b0000),
        //.DIVF(7'b1010101),
        //.DIVQ(3'b011)
        // == 141 == (breakdown -- resulting oscillation is stable but only 35.25MHz)
        //.DIVR(4'b0000),
        //.DIVF(7'b0101110),
        //.DIVQ(3'b010)
        // == 165 MHz == (breakdown -- resulting oscillation is stable but only 41.25MHz)
        //.DIVR(4'b0000),
        //.DIVF(7'b0110110),
        //.DIVQ(3'b010)
    ) clk_pll (
        .PACKAGEPIN(CLK_12MHZ),
        .PLLOUTGLOBAL(clk),
        .LOCK(clk_lock),
        .RESETB(1'b1),
        .BYPASS(1'b0)
    );

    always @(posedge clk or negedge clk_lock) begin
        if (!clk_lock) begin
            P1A1 <= 1'b0;
        end else begin
            P1A1 <= !P1A2;
        end
    end

endmodule

#
# ring.RP2040.py
#    RP2040 ring oscillator test
#    connect P1 and P2
#
# Neil Gershenfeld 12/25/22
#
# This work may be reproduced, modified, distributed,
# performed, and displayed for any purpose, but must
# acknowledge this project. Copyright is retained and
# must be preserved. The work is provided as is; no
# warranty is provided, and users accept all liability.
#
from machine import Pin

@micropython.native
def run():
   p1 = Pin(1,Pin.IN)
   p2 = Pin(2,Pin.OUT)
   while (True):
       p2(not p1())

machine.freq(250000000)
run()

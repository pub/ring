//
// ring.RP2040.ino
//    RP2040 ring oscillator test
//    connect P1 and P2
//
// Neil Gershenfeld 12/26/22
//
// This work may be reproduced, modified, distributed,
// performed, and displayed for any purpose, but must
// acknowledge this project. Copyright is retained and
// must be preserved. The work is provided as is; no
// warranty is provided, and users accept all liability.
//

#define digitalWriteFast(pin,val) (val ? sio_hw->gpio_set = (1 << pin) : sio_hw->gpio_clr = (1 << pin))
#define digitalReadFast(pin) ((1 << pin) & sio_hw->gpio_in)

void setup() {
   //
   // nothing to do in first core
   //
   }

void loop() {
   }

void setup1() {
   //
   // use second core to avoid Arduino interrupts
   //
   pinMode(2,OUTPUT);
   }

void loop1() {
   //
   // direct port I/O version
   //    12.9 MHz at 250 MHz clock
   //    9.17 MHz at 133 MHz clock
   //
   uint32_t pin1 = (1 << 1);
   uint32_t pin2 = (1 << 2);
   /*
   while (1) {
      if (pin1 & sio_hw->gpio_in)
         sio_hw->gpio_clr = pin2;
      else
         sio_hw->gpio_set = pin2;
      }
   */
   //
   // Arduino Fast version
   //    12.9 MHz at 250 MHz clock
   //    9.17 MHz at 133 MHz clock
   //
   /**/
   while (1) {
      if (digitalReadFast(1))
         digitalWriteFast(2,LOW);
      else
         digitalWriteFast(2,HIGH);
      }
   /**/
   //
   // Arduino version
   //    1.11 MHz at 250 MHz clock
   //    584 kHz at 133 MHz clock
   //
   /*
   while (1) {
      if (digitalRead(1))
         digitalWrite(2,LOW);
      else
         digitalWrite(2,HIGH);
      }
   */
   }

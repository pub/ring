//
// ring.128DB32.c
//
// 128DB32 ring oscillator test
//
// Neil Gershenfeld 1/9/21
//
// This work may be reproduced, modified, distributed,
// performed, and displayed for any purpose, but must
// acknowledge this project. Copyright is retained and
// must be preserved. The work is provided as is; no
// warranty is provided, and users accept all liability.
//

#include <avr/io.h>

int main(void) {
   CPU_CCP = CCP_IOREG_gc; // unprotect clock
   CLKCTRL.OSCHFCTRLA = 9 << 2; // select 24 MHz clock
   PORTD.DIRSET = PIN5_bm;
   while (1) {
      if (VPORTD.IN & PIN6_bm)
            VPORTD.OUT &= ~PIN5_bm;
         else
            VPORTD.OUT |= PIN5_bm;
      }
   }

//
// ring.ESP32C3.ino
//    ESP32C3 ring oscillator test
//    connect GPIO 8 and 20
//
// Neil Gershenfeld 12/26/22
//
// This work may be reproduced, modified, distributed,
// performed, and displayed for any purpose, but must
// acknowledge this project. Copyright is retained and
// must be preserved. The work is provided as is; no
// warranty is provided, and users accept all liability.
//

uint32_t input,output;

void setup() {
   pinMode(8,OUTPUT);
   output = (1 << 8);
   input = (1 << 20);
   }

void loop() {
   //
   // direct port I/O version
   //    3.79 MHz at 160 MHz clock
   //
   /**/
   while (1) {
      if (GPIO.in.val & input)
         GPIO.out_w1tc.val = output;
      else
         GPIO.out_w1ts.val = output;
      }
   /**/
   //
   // Arduino version
   //    736 kHz at 166 MHz clock
   //
   /*
   while (1) {
      if (digitalRead(20))
         digitalWrite(8,LOW);
      else
         digitalWrite(8,HIGH);
      }
   */
   }

//
// ring.ESP32.ino
//
// ESP32 ring oscillator test
//
// Neil Gershenfeld 12/6/20
//
// This work may be reproduced, modified, distributed,
// performed, and displayed for any purpose, but must
// acknowledge this project. Copyright is retained and
// must be preserved. The work is provided as is; no
// warranty is provided, and users accept all liability.
//

uint32_t input,output;

void setup() {
   setCpuFrequencyMhz(240);
   output = 1 << 16;
   input = 1 << 17;
   GPIO.enable_w1ts = output;
   }

void loop() {
   while (1) {
      //
      // GPIO: 3.81 MHz
      //
      if (GPIO.in & input)
         GPIO.out_w1tc = output;
      else
         GPIO.out_w1ts = output;
      }
   }

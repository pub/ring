#
# ring.gpiozero.py
#    Raspberry Pi gpiozero ring oscillator test
#
# Neil Gershenfeld 3/21/21
#
# This work may be reproduced, modified, distributed,
# performed, and displayed for any purpose, but must
# acknowledge this project. Copyright is retained and
# must be preserved. The work is provided as is; no
# warranty is provided, and users accept all liability.
#
from gpiozero import LED
from gpiozero import Button
led = LED(23)
button = Button(24)
while (True):
   if button.is_pressed:
      led.on()
   else:
      led.off()

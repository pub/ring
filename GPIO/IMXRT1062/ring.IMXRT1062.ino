//
// ring.IMXRT1062.ino
//
// ring oscillator test
//
// Neil Gershenfeld 8/12/21
//
// This work may be reproduced, modified, distributed,
// performed, and displayed for any purpose, but must
// acknowledge this project. Copyright is retained and
// must be preserved. The work is provided as is; no
// warranty is provided, and users accept all liability.
//

void setup() {
   pinMode(0,OUTPUT);
   }

void loop() {
   while (1) {
      uint8_t data = GPIO6_DR;
      if (data & 0b100) // pin 1
         GPIO6_DR = 0;
      else
        GPIO6_DR = 0b1000; // pin 0
      }
   }

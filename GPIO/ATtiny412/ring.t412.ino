//
// ring.t412.ino
//
// ATtiny412 ring oscillator test
//
// Neil Gershenfeld 11/13/20
//
// This work may be reproduced, modified, distributed,
// performed, and displayed for any purpose, but must
// acknowledge this project. Copyright is retained and
// must be preserved. The work is provided as is; no
// warranty is provided, and users accept all liability.
//

void setup() {
   CPU_CCP = CCP_IOREG_gc; // unprotect clock
   CLKCTRL.MCLKCTRLB = 0; // turn off prescalar (20 MHz)
   PORTA.DIRSET = PIN6_bm; // set output pin
   }

void loop() {
   while(1) {
      //
      // VPORT: 1.808 MHz
      //    250 ns high (5 cycles), 300 ns low (6 cycles)
      //
      if (VPORTA.IN & PIN7_bm)
         VPORTA.OUT &= ~PIN6_bm;
      else
         VPORTA.OUT |= PIN6_bm;
      //
      // PORT: 1.056 MHz
      //
      /*
      if (PORTA.IN & PIN7_bm)
         PORTA.OUTCLR = PIN6_bm;
      else
         PORTA.OUTSET = PIN6_bm;
      */
      //
      // digitalRead/Write: 0.331 MHz
      //
      /*
      if (digitalRead(1))
         digitalWrite(0,LOW);
      else
         digitalWrite(0,HIGH);
      */
      //
      // digitalReadFast/WriteFast: 1.808 MHz
      //
      /*
      if (digitalReadFast(1))
         digitalWriteFast(0,LOW);
      else
         digitalWriteFast(0,HIGH);
      */
      }
   }

#
# ring.RPi.GPIO.py
#    Raspberry Pi RPI.GPIO ring oscillator test
#
# Neil Gershenfeld 3/21/21
#
# This work may be reproduced, modified, distributed,
# performed, and displayed for any purpose, but must
# acknowledge this project. Copyright is retained and
# must be preserved. The work is provided as is; no
# warranty is provided, and users accept all liability.
#
import RPi.GPIO as GPIO
pinout = 16
pinin = 18
GPIO.setwarnings(False)
GPIO.setmode(GPIO.BOARD)
GPIO.setup(pinout,GPIO.OUT)
GPIO.setup(pinin,GPIO.IN)
while (True):
   if (GPIO.input(pinin) == GPIO.LOW):
      GPIO.output(pinout,GPIO.HIGH)
   else:
      GPIO.output(pinout,GPIO.LOW)

//
// ring.libgpiod.c
//
// libgpiod ring oscillator test
//    gcc ring.libgpiod.c -o ring.libgpiod -lgpiod
//
// Neil Gershenfeld 12/13/20
//
// This work may be reproduced, modified, distributed,
// performed, and displayed for any purpose, but must
// acknowledge this project. Copyright is retained and
// must be preserved. The work is provided as is; no
// warranty is provided, and users accept all liability.
//
#include <gpiod.h>
int main(int argc, char **argv) {
   struct gpiod_chip *chip;
   struct gpiod_line *lineout;
   struct gpiod_line *linein;
   unsigned int pinout = 23;
   unsigned int pinin = 24;
   chip = gpiod_chip_open_by_name("gpiochip0");
   lineout = gpiod_chip_get_line(chip,pinout);
   gpiod_line_request_output(lineout,"main",0);
   linein = gpiod_chip_get_line(chip,pinin);
   gpiod_line_request_input(linein,"main");
   while (1) {
      if (gpiod_line_get_value(linein))
         gpiod_line_set_value(lineout,0);
      else
         gpiod_line_set_value(lineout,1);
      }
   }

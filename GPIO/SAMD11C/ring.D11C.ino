//
// ring.D11C.ino
//
// SAMD11C ring oscillator test
//
// Neil Gershenfeld 11/13/20
//
// This work may be reproduced, modified, distributed,
// performed, and displayed for any purpose, but must
// acknowledge this project. Copyright is retained and
// must be preserved. The work is provided as is; no
// warranty is provided, and users accept all liability.
//

void setup() {
   SYSCTRL->OSC8M.bit.PRESC = 0; // turn off prescalar (48 MHz)
   REG_PORT_DIR0 = PORT_PA02; // set output pin
   PORT->Group[0].PINCFG[4].reg = PORT_PINCFG_INEN; // turn on input pin
   PORT->Group[0].CTRL.reg = PORT_PA04; // turn on continuous sampling
   }

void loop() {
  while (1) {
      //
      // IOBUS: 2.528 MHz
      //
      if (PORT_IOBUS->Group[0].IN.reg & PORT_PA04)
         PORT_IOBUS->Group[0].OUTCLR.reg = PORT_PA02;
      else
         PORT_IOBUS->Group[0].OUTSET.reg = PORT_PA02;
      //
      // PORT: 1.171 MHz
      //
      /*
      if (REG_PORT_IN0 & PORT_PA04)
         REG_PORT_OUTCLR0 = PORT_PA02;
      else
         REG_PORT_OUTSET0 = PORT_PA02;
      */
      }
   }

/*
 * xmega-eventsys-ring.c
 *
 * Created: 7/24/2018 9:13:26 AM
 * Author : Jake
 */ 

#include <avr/io.h>

void clock_init(void){
	// I'm using this on a board with a 16MHz external clock
	OSC.XOSCCTRL = OSC_XOSCSEL_XTAL_256CLK_gc | OSC_FRQRANGE_12TO16_gc; // select external source
	OSC.CTRL = OSC_XOSCEN_bm; // enable external source
	while(!(OSC.STATUS & OSC_XOSCRDY_bm)); // wait for external
	OSC.PLLCTRL =  OSC_PLLSRC_XOSC_gc | 3; // select external osc for pll, do pll = source * 3
	OSC.CTRL |= OSC_PLLEN_bm; // enable PLL
	while (!(OSC.STATUS & OSC_PLLRDY_bm)); // wait for PLL to be ready
	CCP = CCP_IOREG_gc; // enable protected register change
	CLK.CTRL = CLK_SCLKSEL_PLL_gc; // switch to PLL for main clock
	
	// if you want to set-up with the internal 2MHz osc, use this code (from Neil)
	/*
	OSC.PLLCTRL = OSC_PLLFAC4_bm | OSC_PLLFAC3_bm; // 2 MHz * 24 = 48 MHz
	OSC.CTRL = OSC_PLLEN_bm; // enable PLL
	while (!(OSC.STATUS & OSC_PLLRDY_bm)); // wait for PLL to be ready
	CCP = CCP_IOREG_gc; // enable protected register change
	CLK.CTRL = CLK_SCLKSEL_PLL_gc; // switch to PLL
	*/
}

/*
this runs for ~12 cycles at 18MHz, and then dissapears.
*/

int main(void)
{
	clock_init();
	PORTE.DIRSET = PIN7_bm; // this is ATK0-TX 
	
	PORTF.DIRCLR = PIN4_bm; // this is ATK1-CLKIN
	PORTF.PIN4CTRL = PORT_ISC_FALLING_gc; // sense when falling
	
	// setup event channel 0 to have EVSYS_CHMUX_PORTD_PIN4_gc as input
	EVSYS_CH0MUX = EVSYS_CHMUX_PORTF_PIN4_gc;
	EVSYS_CH0CTRL = EVSYS_DIGFILT_1SAMPLE_gc;
	// it seems we can only output events directly on pin7 of ports c, d or e :| (or pin 4 on the same ports)
	// so output event 0 on port E, pin 7
	
	PORTCFG_CLKEVOUT = PORTCFG_EVOUT_PE7_gc;
	
	// kick the oscillator
	EVSYS.STROBE = 1;
	
	uint32_t tck;
	
    while (1) 
    {
		tck ++;
		if(tck % 256){
			PORTA.OUTTGL = PIN0_bm;
		}
		//EVSYS.STROBE = 1; // strobe event channel 0
		//if(PORTF.IN & PIN4_bm){
		//	PORTF.OUTCLR = PIN0_bm;
		//} else {
		//	PORTF.OUTSET = PIN0_bm;
		//}
    }
}


# Round trip timing of BLE UART Service
# Sam Calisch, 2017
# Adapted from Tony DiCola's BLE UART example: https://github.com/adafruit/Adafruit_Python_BluefruitLE
import Adafruit_BluefruitLE
from Adafruit_BluefruitLE.services import UART
import time

ble = Adafruit_BluefruitLE.get_provider()

def main():
    ble.clear_cached_data()
    adapter = ble.get_default_adapter()
    adapter.power_on()
    print('Using adapter: {0}'.format(adapter.name))
    UART.disconnect_devices()
    print('Searching for UART device...')
    try:
        adapter.start_scan()
        device = UART.find_device()
        if device is None:
            raise RuntimeError('Failed to find UART device!')
    finally:
        adapter.stop_scan()
    print('Connecting to device...')
    device.connect()  
    
    try:
        print('Discovering services...')
        UART.discover(device)
        uart = UART(device)
        print "Measuring"
        i = 0
        N = 100
        t0 = time.time();
        times = []
        #we run 2N trials
        #the first N trials wait for a response from the ble uart
        #the second N trials don't wait for a response, in order to time the host side of the loop
        while(i<2*N):
            uart.write('\n') #write newline to the TX characteristic.
            received = uart.read(timeout_sec=10) if i<N else 10
            if received is not None:
                times.append( time.time()-t0 )
                t0 = time.time()
            i = i+1
    finally:
        device.disconnect() #disconnect on exit.
        print "Disconnected"
        print times

ble.initialize()
ble.run_mainloop_with(main)

#include <bluefruit.h>

BLEDis  bledis;
BLEUart bleuart;
uint8_t ch;

void setup()
{
  Serial.begin(115200);
  Serial.println("Bluefruit52 BLEUART Example");

  Bluefruit.begin();
  Bluefruit.setName("Bluefruit52");
  Bluefruit.setConnectCallback(connect_callback);
  Bluefruit.setDisconnectCallback(disconnect_callback);

  // Configure and Start Device Information Service
  bledis.setManufacturer("Adafruit Industries");
  bledis.setModel("Bluefruit Feather52");
  bledis.begin();

  bleuart.begin();  // Configure and Start BLE Uart Service
  setupAdv();  // Set up Advertising Packet
  Bluefruit.Advertising.start();   // Start Advertising

  while(1){
    // echo BLEUART
    if ( bleuart.available() )
    {
      ch = (uint8_t) bleuart.read();
      bleuart.write(&ch,1);
    }
  }
}

void setupAdv(void)
{
  Bluefruit.Advertising.addFlags(BLE_GAP_ADV_FLAGS_LE_ONLY_GENERAL_DISC_MODE);
  Bluefruit.Advertising.addTxPower();
  // Include bleuart 128-bit uuid
  Bluefruit.Advertising.addService(bleuart);
  // There is no room for Name in Advertising packet, so use Scan response for Name
  Bluefruit.ScanResponse.addName();
}

void loop(){}

void connect_callback(void) {Serial.println("Connected");}
void disconnect_callback(uint8_t reason)
{
  (void) reason;
  Serial.println();
  Serial.println("Disconnected");
  Serial.println("Bluefruit will start advertising again");
}

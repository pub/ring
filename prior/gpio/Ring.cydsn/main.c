//
// psoc.ring.c
//
// PSoC 5LP hello-world ring oscillator
//    solder bridge pins:
//      3.3 1.7     Control register ring oscilator 1
//      3.4 1.6     Control register ring oscillator 2
//      3.5 1.5     Software ring oscillator
//      3.6 1.4     Hardware ring oscillator 1
//      12.2 12.3   Hardware ring oscillator 2
//
// Software ring oscillators are enabled by compile flags below.
// Hardware ring oscillators are always on.
//      HW1 is 12.4MHz when double buffered with a 74.7MHz core (3 core cycles per IOP)
//      HW2 is 28MHz.  It is not synced to the core and tends to be unstable.
//
// Eric VanWyk
// 12/5/15
//
// (c) Massachusetts Institute of Technology 2015
// This work may be reproduced, modified, distributed,
// performed, and displayed for any purpose. Copyright is
// retained and must be preserved. The work is provided
// as is; no warranty is provided, and users accept all 
// liability.
//
#include <project.h>
#define USE_API
//#define USE_DATAREGISTER_1
//#define USE_DATAREGISTER_2
//#define USE_CONTROLREGISTER

int main()
{
    CyGlobalIntEnable; /* Enable global interrupts. */
    //
#ifdef USE_API
    for(;;)
    {
        // 0.91MHz (1.82MIOPS) without Inlining
        // 1.97MHz (9.94MIOPS) with Inlining
        SW_OUT_Write(~ SW_IN_Read());
    }
#endif
#ifdef USE_DATAREGISTER_1
    for(;;)
    {
        // 1.58MHz (3.16MIOPS) in Release
        // 2.37MHz (4.74MIOPS) in Debug
        SW_OUT_DR=(SW_OUT_DR &~ SW_OUT_MASK)| ((SW_IN_MASK&~SW_IN_PS) >> (SW_IN_SHIFT-SW_OUT_SHIFT));
    }    
#endif
#ifdef USE_DATAREGISTER_2
    for(;;)
    {
        // 2.15MHz (4.30MIOPS) in Release
        // 1.36MHz (2.72MIOPS) in Debug
        SW_OUT_DR=(SW_IN_PS&SW_IN_MASK)? SW_OUT_DR &~SW_OUT_MASK : SW_OUT_DR | SW_OUT_MASK;
    }    
#endif
#ifdef USE_CONTROLREGISTER
    for(;;)
    {
        
        // 2.15MHz (3.16MIOPS) in Release (Size)
        // 1.48MHz (4.60MIOPS) in Release (Speed)
        // 1.97MHz (3.06MIOPS) in Debug
     Control_Reg_Control = ~Status_Reg_Status;   
    }
    #endif
}

/* [] END OF FILE */

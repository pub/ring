//build:
// gcc -o ring ring.c -l bcm2835
//run:
// sudo ./ring
#include <bcm2835.h>
#include <stdio.h>
#define PIN_OUT RPI_GPIO_P1_11
#define PIN_IN RPI_GPIO_P1_15

int main(int argc, char **argv)
{
    if (!bcm2835_init())
      return 1;

    //set changing pad current can change ringing characteristics on pulses
    bcm2835_gpio_set_pad(BCM2835_PAD_GROUP_GPIO_0_27, BCM2835_PAD_DRIVE_6mA);

    //this block is supposed to prevent kernel swapping
    //I didn't get it to compile...
    //struct sched_param sp;
    //memset(&sp, 0, sizeof(sp));
    //sp.sched_priority = sched_get_priority_max(SCHED_FIFO);
    //sched_setscheduler(0, SCHED_FIFO, &sp);
    //mlockall(MCL_CURRENT | MCL_FUTURE);

    bcm2835_gpio_fsel(PIN_OUT, BCM2835_GPIO_FSEL_OUTP);
    bcm2835_gpio_fsel(PIN_IN , BCM2835_GPIO_FSEL_INPT);
    //bcm2835_gpio_set_pud(PIN_IN, BCM2835_GPIO_PUD_UP);
    while (1)
        bcm2835_gpio_write(PIN_OUT, 1-bcm2835_gpio_lev(PIN_IN) );
    bcm2835_close();
    return 0;
}


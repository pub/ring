#define portout NRF_GPIO->OUT
#define portin NRF_GPIO->IN
#define pin_in 11
#define mask_in 1 << pin_in
#define pin_out 12
#define mask_out 1 << pin_out

void setup() {
  pinMode(pin_out, OUTPUT);
  NRF_GPIO->PIN_CNF[pin_in] = 0; //set input, no pullup/down, standard drive, no sense
  while(1){
     portin & mask_in ? NRF_GPIO->OUTCLR = mask_out : NRF_GPIO->OUTSET = mask_out; //218ns period
  }
}

void loop() {}

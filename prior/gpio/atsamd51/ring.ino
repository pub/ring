#define portin PORT->Group[PORTA].IN.reg
#define outclr PORT->Group[PORTA].OUTCLR.reg
#define outset PORT->Group[PORTA].OUTSET.reg
#define out PORT->Group[PORTA].OUT.reg
#define outtgl PORT->Group[PORTA].OUTTGL.reg

//11:     { PORTA, 21, PIO_TCC_PDEC, (PIN_ATTR_DIGITAL|PIN_ATTR_PWM|PIN_ATTR_TIMER), No_ADC_Channel, PWM0_CH1, TCC0_CH1, EXTERNAL_INT_5 },
const uint8_t in_pin = 21;
const uint32_t in_mask = 1ul<<in_pin;
//12:     { PORTA, 22, PIO_TCC_PDEC, (PIN_ATTR_DIGITAL|PIN_ATTR_PWM|PIN_ATTR_TIMER), No_ADC_Channel, PWM0_CH2, TCC0_CH2, EXTERNAL_INT_6 },
const uint8_t out_pin = 22;
const uint32_t out_mask = 1ul<<out_pin;
//led:{ PORTA, 23, PIO_TCC_PDEC, (PIN_ATTR_DIGITAL|PIN_ATTR_PWM|PIN_ATTR_TIMER), No_ADC_Channel, PWM0_CH3, TCC0_CH3, EXTERNAL_INT_7 }, // TCC2/WO[1]
//const uint32_t out_mask = 1ul<<23;

void setup() {
  //set clock port for digital io
  //PM->APBBMASK.reg |= PM_APBBMASK_PORT ;

  
  // Set pin mode according to chapter '32.6.3 I/O Pin Configuration'
  // enable input, to support reading back values, with pullups disabled
  PORT->Group[PORTA].PINCFG[out_pin].reg = (uint8_t)(PORT_PINCFG_INEN) ;
  // Set pin to output mode  
  PORT->Group[PORTA].DIRSET.reg = out_mask;
  // Set in pin to input mode
  PORT->Group[PORTA].PINCFG[in_pin].reg=(uint8_t)(PORT_PINCFG_INEN) ;
  PORT->Group[PORTA].DIRCLR.reg = in_pin ;
  PORT->Group[PORTA].CTRL.reg = in_mask; //make sure port synchronizer always active


  while(1){
    /*PORT->Group[PORTA].OUTSET.reg = out_mask; // turn on led
    delay(1000);              // wait for a second
    PORT->Group[PORTA].OUTCLR.reg = out_mask; // turn off led
    delay(1000);              // wait for a second
    */
    
    //ternary operator: assymetric 300ns (200-100)
    //portin & in_mask ? outclr = out_mask : outset = out_mask;
    //portin & in_mask ? outclr = out_mask : out = out_mask;
    out = ~(portin);

    
    //bitwise xor: 300ns
    //out = ((portin ^ in_mask) << 1);

    //if else
    /*if (portin & in_mask){
      outclr = out_mask;
    } else {
      outset = out_mask;
    }*/
    
    //toggle, 10 mhz
    //outtgl = out_mask;

    //manual toggle, 20mhz
    //outclr = out_mask;
    //outset = out_mask;

    
  }
  

}
 
void loop() {}

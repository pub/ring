/*
   I found most of these definitions in the arduino cores in
   AppData/Local/Arduino15/packages/adafruit/hardware/samd/1.0.19/cores/arduino/wiring_digital.c & wiring_digital.h
*/

#define PIN_LED_PORT 0
#define PIN_LED_PIN 17

#define PIN_10_PORT 0
#define PIN_10_PIN 18

#define PIN_11_PORT 0
#define PIN_11_PIN 16

uint32_t ulPin = 11;

void setup() {
  // set a pin to in / out ... starting
  PORT->Group[g_APinDescription[ulPin].ulPort].PINCFG[g_APinDescription[ulPin].ulPin].reg = (uint8_t)(PORT_PINCFG_INEN) ;
  PORT->Group[g_APinDescription[ulPin].ulPort].DIRSET.reg = (uint32_t)(1 << g_APinDescription[ulPin].ulPin) ;

  //pinMode(10, OUTPUT);
  //pinMode(11, INPUT);

}

void loop() {
  EPortType port = g_APinDescription[ulPin].ulPort;
  uint32_t pin = g_APinDescription[ulPin].ulPin;
  uint32_t pinMask = (1ul << pin);

  Serial.print("11, port: ");
  Serial.print(port);
  Serial.print(" pin: ");
  Serial.print(pin);
  Serial.println("");
  PORT->Group[port].OUTSET.reg = pinMask;
  delay(100);
  PORT->Group[port].OUTCLR.reg = pinMask;
  delay(100);
}

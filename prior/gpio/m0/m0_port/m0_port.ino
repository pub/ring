/*
   I found most of these definitions in the arduino cores in
   AppData/Local/Arduino15/packages/adafruit/hardware/samd/1.0.19/cores/arduino/wiring_digital.c & wiring_digital.h
   also samd/1.0.19/variants/variant.h and variant.cpp and pins_arduino.h
*/

#define PIN_10_PORT 0
#define PIN_10_PIN 18

#define PIN_11_PORT 0
#define PIN_11_PIN 16

#define RING_PORT_OUTSET PORT->Group[PIN_10_PORT].OUTSET.reg
#define RING_PORT_OUTCLR PORT->Group[PIN_10_PORT].OUTCLR.reg
#define RING_PORT_OUT_MASK (1UL << PIN_10_PIN)

#define RING_PORT_IN PORT->Group[PIN_11_PORT].IN.reg
#define RING_PORT_IN_MASK (1UL << PIN_11_PIN)

void setup() {
  // set 10 to output
  PORT->Group[PIN_10_PORT].DIRSET.reg = (uint32_t)(1 << PIN_10_PIN);

  // set 11 to input
  PORT->Group[PIN_11_PORT].PINCFG[PIN_11_PIN].reg = (uint8_t)(PORT_PINCFG_INEN);

  // do loop
  while (1) { // no loop just c
    RING_PORT_IN & RING_PORT_IN_MASK ? RING_PORT_OUTCLR = RING_PORT_OUT_MASK : RING_PORT_OUTSET = RING_PORT_OUT_MASK;
  }
}

void loop() {

}

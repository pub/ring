#!/usr/bin/env python
#
# ring.pySerial.py
#
# pySerial communication ring test
#
# Neil Gershenfeld 3/31/21
#
# This work may be reproduced, modified, distributed,
# performed, and displayed for any purpose, but must
# acknowledge this project. Copyright is retained and
# must be preserved. The work is provided as is; no
# warranty is provided, and users accept all liability.
#

import serial,sys,time

nloop = 10

if (len(sys.argv) != 3):
   print("command line: ring.pyserial.py serial_port port_speed")
   sys.exit()
port = sys.argv[1]
speed = int(sys.argv[2])

ser = serial.Serial(port,speed)
ser.flushInput()
x = 0
start = time.time()
for i in range(nloop):
   while True:
      ser.write(x.to_bytes(1,byteorder='big'))
      x = int.from_bytes(ser.read(1),byteorder='big')
      if (x == 0):
         break
end = time.time()
print(f"byte ring cycles per second: {nloop*256/(end-start):6.3g}")


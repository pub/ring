//
// ring.SerialUSB.ino
//
// SerialUSB communication ring test
//
// Neil Gershenfeld 3/31/21
//
// This work may be reproduced, modified, distributed,
// performed, and displayed for any purpose, but must
// acknowledge this project. Copyright is retained and
// must be preserved. The work is provided as is; no
// warranty is provided, and users accept all liability.
//

void setup() {
   SerialUSB.begin(0);
   }

void loop() {
   while (true) {
      if (SerialUSB.available()) {
         SerialUSB.write(1+SerialUSB.read());
         }
      }
   }
